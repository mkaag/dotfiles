"=== Some defaults
set nocompatible
filetype off
syntax on

let $TMPDIR = $HOME.'/.tmp'
set shell=/bin/bash

set hidden
set history=1000

"=== Appearance
set title
set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set ruler
set shortmess=atI
set backspace=indent,eol,start " better backspace
set scrolloff=5
set mouse=a

" turn hybrid line number on
set number relativenumber
set nu rnu


"=== Search
set hlsearch
set incsearch
set ignorecase
set smartcase
" \n will unhighlight search
nmap <silent> <leader>n :silent :nohlsearch<CR>


"=== Smart indenting
set tabstop=4    " Set the default tabstop
set softtabstop=4
set shiftwidth=4 " Set the default shift width for indents
set expandtab   " Make tabs into spaces (set by tabstop)
set smarttab " Smarter tab levels
set autoindent
set cindent
set cinoptions=:s,ps,ts,cs
set cinwords=if,else,while,do,for,switch,case
set wildignore+=*.pyc,*/venv/*,*/s/*


"=== Shortcuts
set pastetoggle=<F3>
nmap ; :Buffers<CR>
"" map ; :File<CR>
nmap <Leader>t :Files<CR>
nmap <Leader>r :Tags<CR>
map <F4> :source ~/.vimrc<CR>
map <F5> :edit ~/.vimrc<CR>
cmap w!! w !sudo tee % >/dev/null
" \s for seeing tabs spaces
nmap <silent> <leader>s :set nolist!<CR>
vmap <C-c> "+y

"=== Syntax highlighting
"autocmd BufReadPost *.sgr set syntax=pug
autocmd BufNewFile,BufRead *.sgr set filetype=pug

"=== Theme
colorscheme mysticaltutor


"=== Plugins
filetype off " required!
so ~/.vim/plugins.vim

" Arduino
let g:arduino_cmd = '/usr/share/arduino/arduino'
let g:arduino_dir = '/usr/share/arduino'
let g:arduino_home_dir = $HOME . "/.arduino15"
let g:arduino_serial_baud = 9600

" NerdTree
let NERDTreeShowHidden=1

" highlight
highlight GitGutterAdd ctermfg=green ctermbg=NONE
highlight GitGutterChange ctermfg=yellow ctermbg=NONE
highlight GitGutterDelete ctermfg=red ctermbg=NONE
highlight GitGutterChangeDelete ctermfg=yellow ctermbg=NONE

" lightline
set laststatus=2   " Always show the statusline
set encoding=utf-8 " Necessary to show unicode glyphs
set t_Co=256 " Terminal colors
let g:lightline = {}
let g:lightline.component_expand = {
    \   'linter_checking': 'lightline#ale#checking',
    \   'linter_warnings': 'lightline#ale#warnings',
    \   'linter_errors': 'lightline#ale#errors',
    \   'linter_ok': 'lighline#ale#ok',
    \   }
let g:lightline.component_type = {
    \   'linter_checking': 'left',
    \   'linter_warnings': 'warning',
    \   'linter_errors': 'error',
    \   'linter_ok': 'left',
    \   }
let g:lightline.component_function = {
    \   'gitbranch': 'gitbranchname()'
    \   }
let g:lightline.colorscheme = 'solarized'
let g:lightline.active = {
    \  'left': [ ['mode', 'paste' ], ['readonly', 'filename', 'modified'] ],
    \  'right': [
    \       ['linter_checking', 'linter_warnings', 'linter_errors', 'linter_ok'],
    \       ['lineinfo'], ['percent'], ['fileformat', 'fileencoding']
    \   ]
    \ }

" fzf + ag
if executable('ag')
    let g:ackprg = 'ag --vimgrep'
endif
" noremap <C-p> :Files<cr>
" let g:fzf_actions = {
"    \ 'ctrl-t': 'tab split',
"    \ 'ctrl-i': 'split',
"    \ 'ctrl-s': 'vsplit' }
" let g:fzf_layout = { 'down': '~20%' }
" let g:rg_command = '
"    \ rg --column --line-number --no-heading --fixed-strings --ignore-case --no-ignore --hidden --follow --color "always"
"    \ --glob "*.{graphql,gql,js,json,php,md,styl,jade,pug,html,config,py,cpp,c,go,hs,rb,conf}
"    \ --glob "!{.git,.nvm,node_modules,vendor}/*" '
" command! -bang -nargs=* F call fzf#vim#grep(g:rg_command . shellescape(<q-args>), 1, <bang>0)
" command! -bang -nargs=* FU call fzf#vim#grep(g:rg_command . '-m1 ' . shellescape(<q-args>), 1, <bang>0)

" ale
let g:ale_sign_column_always = 1
let g:ale_fixers = {
    \ '*': ['remove_trailing_lines', 'trim_whitespace'],
    \ 'html': ['prettier'],
    \ 'javascript': ['eslint', 'prettier'],
    \ 'css': ['stylelint', 'prettier'],
    \ }
let b:ale_linter_aliases = ['javascript', 'vue']
let g:ale_linters = {
    \ 'markdown': ['markdownlint'],
    \ 'pug': ['puglint'],
    \ 'json': ['jsonlint'],
    \ 'sass': ['sasslint'],
    \ 'javascript': ['eslint', 'vls'],
    \ 'typescript': ['tsserver', 'tslint'],
    \ }
let g:ale_pattern_options = {
    \ '.*\.d.ts$': { 'ale_enabled': 0 }
    \ }
let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning = 'W'
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
let g:ale_lint_on_text_changed = 'never'
let g:ale_set_loclist = 0
let g:ale_set_quickfix = 1
nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)

" goyo
let g:goyo_width = '80%'
function! ProseMode()
    call goyo#execute(0, [])
    set spell noci nosi noai nolist noshowmode noshowcmd
    set complete+=s
    set bg=light
    if !has('gui_running')
        let g:solarized_termcolors=256
    endif
    colors solarized
endfunction
command! ProseMode call ProseMode()
nmap \p :ProseMode<CR>

" latex
let g:livepreview_previewer = 'evince'
let g:livepreview_engine = 'xelatex'
let g:livepreview_cursorhold_recompile = 0
