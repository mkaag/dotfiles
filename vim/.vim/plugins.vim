set rtp+=~/.vim/bundle/Vundle.vim
set rtp+=~/.fzf

call vundle#begin()

Plugin 'VundleVim/Vundle.vim'

Plugin 'airblade/vim-gitgutter'
Plugin 'bronson/vim-trailing-whitespace'
Plugin 'easymotion/vim-easymotion'
Plugin 'editorconfig/editorconfig-vim'
Plugin 'itchyny/lightline.vim'
Plugin 'itchyny/vim-gitbranch'
Plugin 'junegunn/goyo.vim'
Plugin 'mattn/emmet-vim'

" Languages
Plugin 'sheerun/vim-polyglot'
Plugin 'jparise/vim-graphql'
Plugin 'tpope/vim-rails'
Plugin 'tpope/vim-bundler'
Plugin 'rstacruz/sparkup'
Plugin 'godlygeek/tabular'

" Features
Plugin 'stevearc/vim-arduino'
Plugin 'ConradIrwin/vim-bracketed-paste'
Plugin 'tpope/vim-commentary'
Plugin 'junegunn/fzf'
Plugin 'junegunn/fzf.vim'
Plugin 'mileszs/ack.vim'
Plugin 'roxma/vim-tmux-clipboard'
Plugin 'tmux-plugins/vim-tmux-focus-events'
Plugin 'reedes/vim-colors-pencil'
Plugin 'scrooloose/nerdtree'
Plugin 'terryma/vim-multiple-cursors'
Plugin 'tpope/vim-eunuch'
Plugin 'tpope/vim-surround'
Plugin 'w0rp/ale'
Plugin 'maximbaz/lightline-ale'
Plugin 'xuhdev/vim-latex-live-preview'

call vundle#end()

filetype plugin indent on
