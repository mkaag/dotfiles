dotfiles
========

Use `stow` program to enable a given package, such as:

```
cd .dotfiles
stow vim
stow mbsync
```
