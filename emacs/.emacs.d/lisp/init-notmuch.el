(autoload 'notmuch "notmuch" "notmuch mail" t)

(use-package notmuch
  :preface (setq-default notmuch-command (executable-find "notmuch"))
  :if (executable-find "notmuch")
  :bind (("<f2>" . notmuch)
      :map notmuch-search-mode-map
      ("t" . my/notmuch-toggle-read)
      ("d" . my/notmuch-toggle-delete)
      ("r" . notmuch-search-reply-to-thread)
      ("R" . notmuch-search-reply-to-thread-sender)
      :map notmuch-show-mode-map
      ("l" . my/notmuch-show-jump-to-latest)
      ("<tab>" . org-next-link)
      ("<backtab>". org-previous-link)
      ("C-<return>" . browse-url-at-point))
  :config
  (defun my/notmuch-toggle-read ()
    "toggle read status of message"
    (interactive)
    (if (member "unread" (notmuch-search-get-tags))
        (notmuch-search-tag (list "-unread"))
        (notmuch-search-tag (list "+unread"))))
  (defun my/notmuch-toggle-delete ()
    "toggle delete status of message"
    (interactive)
    (if (member "deleted" (notmuch-search-get-tags))
        (notmuch-search-tag (list "-deleted"))
        (notmuch-search-tag (list "+deleted"))))
  (defun my/notmuch-show-jump-to-latest ()
    "Jump to the message in the current thread with the latest timestamp."
    (interactive)
    (let ((timestamp 0)
          latest)
     (notmuch-show-mapc
      (lambda () (let ((ts (notmuch-show-get-prop :timestamp)))
                  (when (> ts timestamp)
                    (setq timestamp ts
                        latest (point))))))
     (if latest
        (goto-char latest)
        (error "Cannot find latest message."))))
    )

(setq message-auto-save-directory "~/.local/share/mail/drafts/")
(setq message-send-mail-function 'message-send-mail-with-sendmail)
(setq sendmail-program "~/.local/bin/msmtp-enqueue.sh")
; (setq message-sendmail-extra-arguments '("--read-envelope-from"))
(setq mail-user-agent 'message-user-agent)
(setq message-sendmail-envelope-from 'header)
(setq mail-envelope-from 'header)
(setq mail-specify-envelope-from t)
(setq message-sendmail-f-is-evil nil)
(setq message-kill-buffer-on-exit t)
(setq notmuch-always-prompt-for-sender t)
(setq notmuch-archive-tags '("-inbox" "-unread"))
(setq notmuch-crypto-process-mime t)

; (setq notmuch-hello-sections '(notmuch-hello-insert-header
;                                notmuch-hello-insert-saved-searches
;                                notmuch-hello-insert-search
;                                notmuch-hello-insert-recent-searches
;                                notmuch-hello-insert-alltags
;                                notmuch-hello-insert-footer))

(setq notmuch-labeler-hide-known-labels t)
(setq notmuch-search-oldest-first nil)
(setq notmuch-archive-tags '("-inbox" "-unread"))
(setq notmuch-message-headers '("To" "Cc" "Subject" "Bcc"))

(setq notmuch-saved-searches '((:name "inbox" :query "tag:inbox")
                              (:name "unread" :query "tag:inbox AND tag:unread")
                              (:name "private" :query "tag:inbox AND to:mkaag@me.com OR to:mkaag@bluewin.ch OR to:mkaag@pm.me")
                              (:name "kaag.me" :query "tag:inbox AND to:maurice@kaag.me")
                              (:name "c-mission.com" :query "tag:inbox AND to:maurice@c-mission.com")
                              (:name "clicktodo.com" :query "tag:inbox AND to:maurice.kaag@clicktodo.com")
                              (:name "trivadis.com" :query "tag:inbox AND to:maurice.kaag@trivadis.com")
                              (:name "ALL auto-lemania.ch" :query "auto-lemania.ch OR lemania-auto.ch OR hdinc@bluewin.ch")
                              (:name "ALL dr-bieth.fr" :query "dr-bieth.fr OR jean.philippe5@live.fr OR dr.bieth@gmail.com")
                              (:name "ALL feralsa.ch" :query "feralsa.ch")
                              (:name "ALL lagrangeauxcoqs.com" :query "lagrangeauxcoqs OR bkm.winter@gmail.com")
                              (:name "ALL lambelet-sa.ch" :query "lambelet-sa.ch")
                              (:name "ALL michelekauffmann.fr" :query "michelekauffmann.fr OR m-kauffmann@orange.fr")
                              (:name "ALL netcamstudio.com" :query "netcamstudio.com OR steve@moonware.ch")
                              (:name "ALL on-architecture.ch" :query "on-architecture.ch")
                              (:name "ALL vinsargentins.com" :query "vinsargentins.com OR elsedecker@gmail.com")
                              ))



;; Key bindings
(global-set-key (kbd "C-c m") `notmuch) ;; C-c m opens up Notmuch from any buffer

(eval-after-load 'notmuch-show
  '(define-key notmuch-show-mode-map ";" 'notmuch-show-apply-tag-macro))
(setq notmuch-show-tag-macro-alist
  (list
   '("d" "+deleted" "-inbox")
   '("S" "+spam" "-inbox")
   )
  )
(defun notmuch-show-apply-tag-macro (key)
  (interactive "k")
  (let ((macro (assoc key notmuch-show-tag-macro-alist)))
    (apply 'notmuch-show-tag-message (cdr macro))))

;; Custom counters
(defvar notmuch-hello-refresh-count 0)

(defun notmuch-hello-refresh-status-message ()
 (unless no-display
  (let* ((new-count
          (string-to-number
           (car (process-lines notmuch-command "count"))))
         (diff-count (- new-count notmuch-hello-refresh-count)))
   (cond
    ((= notmuch-hello-refresh-count 0)
     (message "You have %s messages."
      (notmuch-hello-nice-number new-count)))
    ((> diff-count 0)
     (message "You have %s more messages since last refresh."
      (notmuch-hello-nice-number diff-count)))
    ((< diff-count 0)
     (message "You have %s fewer messages since last refresh."
      (notmuch-hello-nice-number (- diff-count)))))
   (setq notmuch-hello-refresh-count new-count))))

(add-hook 'notmuch-hello-refresh-hook 'notmuch-hello-refresh-status-message)

;; Set the initial pointer next to the saved search
(add-hook 'notmuch-hello-refresh-hook
    (lambda ()
     (if (and (eq (point) (point-min))
          (search-forward "Saved searches:" nil t))
      (progn
       (forward-line)
       (widget-forward 1))
      (if (eq (widget-type (widget-at)) 'editable-field)
       (beginning-of-line)))))

(provide 'init-notmuch)
