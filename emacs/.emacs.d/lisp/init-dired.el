; (require-package 'dired+)

; (after-load 'dired
;   (require 'dired+)
;   (toggle-diredp-find-file-reuse-dir 1)
;   (setq dired-recursive-deletes 'top))

; (provide 'init-dired)

(quelpa
 '(quelpa-use-package
   :fetcher git
   :url "https://framagit.org/steckerhalter/quelpa-use-package.git"))
(require 'quelpa-use-package)

(use-package dired+
  :quelpa (dired+ :fetcher url :url "https://www.emacswiki.org/emacs/download/dired+.el")
  :defer 1
  :init
  (setq diredp-hide-details-initially-flag nil)
  (setq diredp-hide-details-propagate-flag nil)

  :config
  (diredp-toggle-find-file-reuse-dir 1))

(provide 'init-dired)
