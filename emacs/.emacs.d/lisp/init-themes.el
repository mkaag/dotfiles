;; custom theme packages
;; can be removed
(add-to-list 'load-path
             (expand-file-name "lisp/custom-themes" user-emacs-directory))
(setq custom-theme-directory
      (expand-file-name "lisp/custom-themes" user-emacs-directory))

; (require 'challenger-deep-overrides-theme)
(require 'dracula-theme)
; (require 'spacemacs-dark-theme)

(defalias 'yes-or-no-p 'y-or-n-p)

(provide 'init-themes)
