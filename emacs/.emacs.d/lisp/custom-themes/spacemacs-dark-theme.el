(require 'spacemacs-common)

(deftheme spacemacs-dark "Spacemacs theme, the dark version")

(create-spacemacs-theme 'dark 'spacemacs-dark)

(provide-theme 'spacemacs-dark)

(provide 'spacemacs-dark-theme)
