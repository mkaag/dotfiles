(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default bold shadow italic underline bold bold-italic bold])
 '(ansi-color-names-vector
   (vector "#d6d6d6" "#c82829" "#718c00" "#eab700" "#4271ae" "#8959a8" "#3e999f" "#4d4d4c"))
 '(custom-safe-themes
   (quote
    ("91ce1e1533921a31778b95d1626371499e42d46833d703e25a07dcb8c283f4af" . t)))
 '(fci-rule-color "#d6d6d6")
 '(flycheck-color-mode-line-face-to-color (quote mode-line-buffer-id))
 '(js-indent-level 2)
 '(magit-commit-arguments nil)
 '(markdown-preview-custom-template "~/.emacs.d/markdown-preview-template.html")
 '(org-hide-emphasis-markers t)
 '(org-modules
   (quote
    (org-bbdb org-bibtex org-docview org-gnus org-habit org-info org-irc org-mhe org-rmail org-w3m)))
 '(package-selected-packages
   (quote
    (doom-themes quelpa-use-package pos-tip markdown-mode hi2 hayoo ghci-completion fuzzy flycheck-hdevtools flycheck-haskell exec-path-from-shell evil dired+ ac-haskell-process)))
 '(safe-local-variable-values (quote ((css-indent-offset . 2) (no-byte-compile t))))
 '(send-mail-function (quote mailclient-send-it)))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((((class color) (min-colors 16777215)) (:background "#282828" :foreground "#fdf4c1")) (((class color) (min-colors 255)) (:background "#262626" :foreground "#ffffaf"))))
 '(term ((t (:foreground "ivory1")))))
